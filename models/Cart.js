// [SECTION] Modules and Dependencies
const mongoose = require("mongoose");

// [SECTION] Schema/Blueprint
const cartSchema = new mongoose.Schema({

	userId: {
		type: String,
		required: [true, "User name is required"]
	},

	addedProducts: {

		basket: [
				{

				productId: {
					type: String,
					required: [true, "Product id is required"]
				},

				productName: {
					type: String,
					required: [true, "Product name is required"]
				},

				productPrice: {
					type: Number,
					required: [true, "Product price is required"]
				},

				productImages: [
					{
						type: String,
						required: [true, "Product image is required"]
					}
				],

				quantity: {
					type: Number,
					required: [true, "Quantity is required"]
				},

				subtotal: {
					type: Number,
					required: [true, "Subtotal is required"]
				}
			}
		],

		totalAmount: {
			type: Number,
			default: 0
		}

	}


})


// [SECTION] Model
module.exports = mongoose.model("Cart", cartSchema);