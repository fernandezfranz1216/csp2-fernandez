// [SECTION] Modules and Dependencies
const mongoose = require("mongoose");

// [SECTION] Schema/Blueprint
const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Product name is required"]
	},

	description: {
		type: String,
		required: [true, "Description is required"]
	},

	price: {
		type: Number,
		required: [true, "Price is required"]
	},

	quantityAvailable: {
		type: Number,
		required: [true, "Quantity is required"]
	},

	images: [
		{
		    type: String,
		    required: [true, "Images is required"]
		}
	],

	isActive: {
		type: Boolean,
		default: true
	},

	createdOn: {
		type: Date,
		default: new Date()
	},

	userOrders: [
		{
			userid: {
				type: String,
				required: "User ID is required"
			},
			purchasedOn: {
				type: Date,
				required: "Date is required"
			}
		}
	]
})


// [SECTION] Model
module.exports = mongoose.model("Product", productSchema);