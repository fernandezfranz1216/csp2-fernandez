// [SECTION] Modules and Dependencies
const mongoose = require("mongoose");

// [SECTION] Schema/Blueprint
const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First name is required"]
	},

	lastName: {
		type: String,
		required: [true, "Last name is required"]
	},

	email: {
		type: String,
		required: [true, "Email is required"]
	},

	password: {
		type: String,
		required: [true, "Password id required"]
	},

	isAdmin: {
		type: Boolean,
		default: false
	},

	mobileNo: {
		type: String,
		required: [true, "Mobile No is required"]
	},

	images: [
		{
		    type: String,
		    required: [true, "Images is required"]
		}
	],


	shippingAddress: [
		{
			streetAddress: {
				type: String,
				required: [true, "Street Address is required"]
			},
			city: {
				type: String,
				required: [true, "City is required"]
			},
			province: {
				type: String,
				required: [true, "Province is required"]
			},
			zipcode: {
				type: String,
				required: [true, "Zipcode is required"]
			},
			country: {
				type: String,
				default: "Philippines"
			}
		}
	],

	orderedProduct: {
		products: [ 
			{
				productId: {
					type: String,
					required: [true, "Product ID is required"]
				},
				productName: {
					type: String,
					required: [true, "Product name is required"]
				},
				productPrice: {
					type: Number,
					required: [true, "Price is required"]
				},
				quantity: {
					type: Number,
					required: [true, "Quantity is required"]
				},
				status: {
					type: String,
					default: "shipped"
				},
				purchasedOn: {
					type: Date,
					default: new Date()
				}
			}
		],

		totalAmount: {
			type: Number,
			default: 0
		}
	}
})

// [SECTION] Model
module.exports = mongoose.model("User", userSchema);