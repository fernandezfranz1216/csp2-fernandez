// [SECTION] Dependencies and Modules
const User = require("../models/User");
const Product = require("../models/Product");
const Cart = require("../models/Cart");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// [SECTION] Controllers

// Add to cart
module.exports.addToCart = async (req, res) => {

	try {

		// Search cart
		let searchCart = await Cart.findOne({ userId: req.user.id });

		// Search product
		let searchProduct = await Product.findById(req.body.productId);

		// Check if productID is already present in the array
		const isProductPresent = searchCart.addedProducts.basket.some(
			(item) => item.productId === req.body.productId
		);

		// if present, add the new quantity to the existing quantity
		if (isProductPresent) {

			// Search for the index of the product to be updated
			const productIndex = searchCart.addedProducts.basket.findIndex(
				(item) => item.productId === req.body.productId
				);

			searchCart.addedProducts.basket[productIndex].quantity += req.body.quantity;
			searchCart.addedProducts.basket[productIndex].subtotal = searchCart.addedProducts.basket[productIndex].productPrice * searchCart.addedProducts.basket[productIndex].quantity;

			// Sum all subtotal in the basket array and assigning result to totalAmount
			searchCart.addedProducts.totalAmount = searchCart.addedProducts.basket.reduce(
				(total, product) => total + product.subtotal,0
			)

			// My ignorant self before I discovered .reduce():
			// searchCart.addedProducts.basket[productIndex].quantity += req.body.quantity;
			// let newSubTotal = searchCart.addedProducts.basket[productIndex].productPrice * req.body.quantity;
			// searchCart.addedProducts.basket[productIndex].subtotal = searchCart.addedProducts.basket[productIndex].productPrice * searchCart.addedProducts.basket[productIndex].quantity; 
			// searchCart.addedProducts.totalAmount += newSubTotal;

		} else {

			let addCart = {
					productId: req.body.productId,
					productName: searchProduct.name,
					productPrice: searchProduct.price,
					productImages: searchProduct.images,
					quantity: req.body.quantity,
					subtotal: searchProduct.price * req.body.quantity
			}

			searchCart.addedProducts.basket.push(addCart);
			searchCart.addedProducts.totalAmount += addCart.subtotal;

		}

		searchProduct.quantityAvailable = req.body.quantityAvailable;

		if (searchProduct.quantityAvailable === 0) {
			searchProduct.isActive = false;
		}

		await searchCart.save();
		await searchProduct.save();

		res.status(200).json({ message: 'Product added to the cart successfully' });

	} catch (error) {
		res.status(500).json({ error: 'Internal Server Error' });
	}

}


// Retrieve all items from cart
module.exports.getCart = async (req, res) => {

	try {

		return Cart.findOne({ userId: req.user.id }).then(result => {

			if (!result) {
			    return res.status(404).json({ error: 'Cart not found' });
			}

			return res.send(result.addedProducts);

		})

	} catch (error) {

		res.status(500).json({ error: 'Internal Server Error' });

	}

}


// Change quantity of product in cart
module.exports.changeQuantity = async (req, res) => {

	try	{
		
		return Cart.findOne({ userId: req.user.id }).then(async (result) => {

			// find index of product to be edited
			const productIndex = await result.addedProducts.basket.findIndex((item) => item.productId === req.body.productId);

			if (productIndex !== -1) {

		        result.addedProducts.basket[productIndex].quantity = req.body.quantity;
		        result.addedProducts.basket[productIndex].subtotal = result.addedProducts.basket[productIndex].productPrice * req.body.quantity;

				// Sum all subtotal in the basket array and assigning result to totalAmount
				result.addedProducts.totalAmount = result.addedProducts.basket.reduce(
					(total, product) => total + product.subtotal,0
				)

				// My ignorant self before I discovered .reduce():
				// result.addedProducts.totalAmount -= result.addedProducts.basket[productIndex].subtotal;
				// result.addedProducts.basket[productIndex].subtotal = result.addedProducts.basket[productIndex].productPrice * req.body.quantity;
				// result.addedProducts.totalAmount += result.addedProducts.basket[productIndex].subtotal;
				// result.addedProducts.basket[productIndex].quantity = req.body.quantity;	


				await result.save();

				if (req.body.quantity === result.addedProducts.basket[productIndex].quantity) {

					res.status(200).json({ message: 'Product quantity updated successfully' });

				} else {
					res.status(500).json({ error: 'Failed to update product quantity' });
				}
			} else {
			        res.status(404).json({
			            error: 'Product not found in the cart',
			        });
			    }

		})



	} catch (error) {
		res.status(500).json({ error: 'Internal Server Error' });
	}

}



// Remove a product from cart
module.exports.removeFromCart = async (req, res) => {

	try {

		return Cart.findOne({ userId: req.user.id }).then(async (result) => {

			// find index of product to be edited
			const productIndex = await result.addedProducts.basket.findIndex((item) => item.productId === req.params.productId);

			await result.addedProducts.basket.splice(productIndex, 1);

			// Sum all subtotal in the basket array and assigning result to totalAmount
			result.addedProducts.totalAmount = result.addedProducts.basket.reduce(
				(total, product) => total + product.subtotal,0
			)

			await result.save();

			const isProductPresent = result.addedProducts.basket.some(
				(item) => item.productId === req.params.productId
			);

			if (isProductPresent) {

				res.status(500).json({ error: 'Failed to remove product from cart' });

			} else {
				res.status(200).json({ message: 'Product removed successfully' });
			}

		})

	} catch (error) {

		res.status(500).json({ error: 'Internal Server Error' });

	}

}



// Checkout a product from cart
module.exports.checkoutFromCart = async (req, res) => {

	try {

		// Process stops here and sends response if user is an ADMIN
		if(req.user.isAdmin){
			return res.send("Action Forbidden");
		}

		let productIndex;

		// Update User's ordered products
		let isUserUpdated = await User.findById( req.user.id ).then(async (user) => {

			let searchProductFromCart = await Cart.findOne({userId: req.user.id});

			// find index of product to be edited
			productIndex = await searchProductFromCart.addedProducts.basket.findIndex((item) => item.productId === req.body.productId); 

			let newOrder = {
				products: [
					{
						productId: searchProductFromCart.addedProducts.basket[productIndex].productId,
						productName: searchProductFromCart.addedProducts.basket[productIndex].productName,
						quantity: searchProductFromCart.addedProducts.basket[productIndex].quantity
					}
				],
				totalAmount: searchProductFromCart.addedProducts.basket[productIndex].subtotal
			}

			user.orderedProduct.push(newOrder);

			await user.save();

			return true;




		})

		if (isUserUpdated !== true){

			return res.send({ message: isUserUpdated });

		}

		let isCartUpdated = await Cart.findOne({userId: req.user.id}).then(async (cart) => {

			// Remove checked-out product from cart
			await cart.addedProducts.basket.splice(productIndex, 1);

			// Sum all subtotal in the basket array and assigning result to totalAmount
			cart.addedProducts.totalAmount = cart.addedProducts.basket.reduce(
				(total, product) => total + product.subtotal, 0
			)

			await cart.save();

			return true;
		})

		if (isCartUpdated !== true){

			return res.send({ message: isUserUpdated });

		}

		// Update Products userOrders
		let isProductUpdated = await Product.findById(req.body.productId).then(async (product) => {

			let userPurchase = {
				userid: req.user.id,
				purchasedOn: new Date()
			}

			product.userOrders.push(userPurchase);

			await product.save();
			return true;

		})

		if (isProductUpdated !== true){

			return res.send({ message: isProductUpdated });

		}

		if(isUserUpdated && isProductUpdated && isCartUpdated){
			return res.send({
				message: "Checkout Successful"
			})
		}


	} catch (error) {

		res.status(500).json({ error: 'Internal Server Error' });

	}

}



// Checkout a product from cart
module.exports.checkoutAllFromCart = async (req, res) => {

	try {

		// Process stops here and sends response if user is an ADMIN
		if(req.user.isAdmin){
			return res.send("Action Forbidden");
		}

		let searchProductFromCart;

		// Update User's ordered products
		let isUserUpdated = await User.findById( req.user.id ).then(async (user) => {

			searchProductFromCart = await Cart.findOne({userId: req.user.id});

			// Iterate the basket array to assign values from cart
			for (const productInBasket of searchProductFromCart.addedProducts.basket) {
			        let newOrder = {

	                    productId: productInBasket.productId,
	                    productName: productInBasket.productName,
	                    productPrice: productInBasket.productPrice,
	                    quantity: productInBasket.quantity

			        };

			    user.orderedProduct.products.push(newOrder);
			    user.orderedProduct.totalAmount += productInBasket.productPrice * productInBasket.quantity;
			}


			await user.save();

			return true;




		})

		if (isUserUpdated !== true){

			return res.send({ message: isUserUpdated });

		}

		// Wait for all promises to be resolved
		await Promise.all(
		    searchProductFromCart.addedProducts.basket.map(async (basketsProduct) => {

		        // Update Products userOrders
		        await Product.findById(basketsProduct.productId).then(async (product) => {
		            let userPurchase = {
		                userid: req.user.id,
		                purchasedOn: new Date()
		            };
		            product.userOrders.push(userPurchase);
		            await product.save();

		        });
		    })
		);

		let isCartUpdated = await Cart.findOne({userId: req.user.id}).then(async (cart) => {

			// Remove checked-out product from cart
			await cart.addedProducts.basket.splice(0, cart.addedProducts.basket.length);

			// Reset total amount to zero
			cart.addedProducts.totalAmount = 0;

			await cart.save();

			return true;
		})

		if (isCartUpdated !== true){

			return res.send({ message: isUserUpdated });

		}


		if(isUserUpdated && isCartUpdated){
			return res.send({
				message: "Checkout Successful"
			})
		}


	} catch (error) {

		console.log("error:" + error);

		res.status(500).json({ error: 'Internal Server Error' });

	}

}