// [SECTION] Dependencies and Modules
const Product = require("../models/Product");
const User = require("../models/User")
const bcrypt = require("bcrypt");
const auth = require("../auth");


// [SECTION] Controllers

// Create a product
module.exports.createProduct = async (req, res) => {
  try {
    // Check if an image was uploaded
    // const images = req.files ? req.files.map(file => file.filename) : [];

    // Create new product object
    const newProduct = new Product({
      name: req.body.name,
      description: req.body.description,
      price: req.body.price,
      quantityAvailable: req.body.quantityAvailable,
      images: req.body.image
    });

    await newProduct.save();

    return res.send(true);
  } catch (error) {
    console.error(error);
    return res.send(false);
  }
};

// Checking out a product
module.exports.checkoutProduct = async (req, res) => {

	// Process stops here and sends response if user is an ADMIN
	if(req.user.isAdmin){
		return res.send("Action Forbidden");
	}

	try {

		// Update User's ordered products
		let isUserUpdated = await User.findById( req.user.id ).then(async (user) => {

			let searchProduct = await Product.findById(req.body.productId); 

			let newOrder = {
				productId: searchProduct._id,
				productName: searchProduct.name,
				productPrice: searchProduct.price,
				quantity: req.body.quantity
			}

			user.orderedProduct.products.push(newOrder);

			user.orderedProduct.totalAmount += searchProduct.price * req.body.quantity;

			await user.save();
			return true;

		})

		if (isUserUpdated !== true){

			return res.send({ message: isUserUpdated });

		}

		// Update Products userOrders
		let isProductUpdated = await Product.findById(req.body.productId).then(async (product) => {

			let userPurchase = {
				userid: req.user.id,
				purchasedOn: new Date()
			}

			product.userOrders.push(userPurchase);
			product.quantityAvailable = req.body.quantityAvailable;

			if (product.quantityAvailable === 0) {
				product.isActive = false;
			}

			await product.save();
			return true;

		})

		if (isProductUpdated !== true){

			return res.send({ message: isProductUpdated });

		}

		if(isUserUpdated && isProductUpdated){
			return res.send({
				message: "Purchase Successful"
			})
		}

	} catch (error) {
		console.error('Update failed:', error);
		res.status(500).json({ error: 'Internal Server Error' });
	}

}


// Retrieve all products
module.exports.retrieveAllProducts = async (req, res) => {

	try{

		return Product.find({}).then(result => {
			return res.send(result);
		})

	} catch (error) {

		res.status(500).json({ error: 'Internal Server Error' });

	}

}



// Retrieve active products
module.exports.retrieveActiveProducts = async (req, res) => {

	try{

		return Product.find({ isActive: true }).then(result => {
			return res.send(result);
		})

	} catch (error) {

		res.status(500).json({ error: 'Internal Server Error' });

	}

}



// Retrieve a single product
module.exports.retrieveAProduct = async (req, res) => {

	try{

		return Product.findById(req.params.productId).then(result => {
			return res.send(result);
		})

	} catch (error) {

		res.status(500).json({ error: 'Internal Server Error' });

	}

}

// Update a product
module.exports.updateProduct = async (req, res) => {

	try{

		console.log(req.files);

		// Check if an image was uploaded
		// const newImages = req.files ? req.files.map(file => file.filename) : [];

		// const existingImages = Array.isArray(req.body.existingImages)
		//             ? req.body.existingImages.join(',')
		//             : req.body.existingImages || '';

		// const existingImagesArray = existingImages.split(',');

		let updatedProduct = {
			name: req.body.name,
			description: req.body.description,
			price: req.body.price,
			quantityAvailable: req.body.quantityAvailable,
			images: req.body.image
		}

		await Product.findByIdAndUpdate(req.params.productId, updatedProduct);

		return res.send(true);

	} catch (error) {

		console.error(error);
		return res.send(false);
		
	}

}



// Archive a product
module.exports.archiveProduct = async (req, res) => {

	try{

		await Product.findByIdAndUpdate(req.params.productId, { isActive: false });

		return res.send(true);

	} catch (error) {

		return res.send(false);

	}

}



// Activate a product
module.exports.activateProduct = async (req, res) => {

	try{

		await Product.findByIdAndUpdate(req.params.productId, { isActive: true });

		return res.status(200).json({ message: "Activation successful" });

	} catch (error) {

		res.status(500).json({ error: "Internal Server Error" })

	}

}