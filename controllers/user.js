// [SECTION] Dependencies and Modules
const User = require("../models/User");
const Product = require("../models/Product");
const Cart = require("../models/Cart");
const bcrypt = require("bcrypt");
const auth = require("../auth");


// [SECTION] Controllers

// User Registration
module.exports.registerUser = async (req, res) => {

	try {
  	// Check if the email or mobileNo is already registered
	  const existingUser = await User.findOne({ email: req.body.email });

	  if (existingUser) {
	    return res.status(400).json({ error: 'Email is already registered' });
	  }

	  const image = req.files ? req.files.map(file => file.filename) : ['defpp.png'];

	  let newUser = new User({
	  	firstName: req.body.firstName,
	  	lastName: req.body.lastName,
	  	email: req.body.email,
	  	mobileNo: req.body.mobileNo,
	  	password: bcrypt.hashSync(req.body.password, 10),
	  	images: image 
	  });

	  // Save the user to the database
	  await newUser.save();

	   // Access the new user's _id
	  const userId = newUser._id;

	  // Create a new cart with the user's _id
	  const newCart = new Cart({ userId });

	  // Save the cart to the database
	  await newCart.save();

	  return res.send(true);

	 } catch (error){

	 	console.log(error);

	 	return res.send(false);

	 }

}



// User Authentication
module.exports.loginUser = (req, res) => {

	// error handling
	try {

		return User.findOne({ email: req.body.email }).then(result => {

			// check if user exists
			if(result == null){

				// return error message
				return res.status(400).json({ error: 'User does not exists' });

			} else {

				const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);

				// Check if the password is correct
				if(isPasswordCorrect) {

					return res.send({ access: auth.createAccessToken(result) })

				} else {

					return res.send(false);
				}

			}

		})

	} catch (error)	{
		res.status(500).json({ error: 'Internal Server Error' });
	}

}

// Retieve all users
module.exports.retrieveAllUsers = async (req, res) => {

	try {

		const result = await User.find({});
		
		return res.send(result);

	} catch (error) {

		console.log("error" + error);

		res.status(500).json({ error: 'Internal Server Error' });

	}

}


// Retrieve user Details
module.exports.userDetails = async (req, res) => {

	try {

		return User.findById(req.user.id).then(result => {
			return res.send(result);
		})

	} catch (error) {
		res.status(500).json({ error: 'Internal Server Error' });
	}

}



// Set user as Admin
module.exports.setAdmin = async (req, res) => {

	try	{

		let isUserAdmin = await User.findByIdAndUpdate(req.params.userId, { isAdmin: true }, { new: true });

		if(isUserAdmin){
			return res.send(true);
		}


	}	catch (error) {
		return res.send(false);
	}

}


// Set user as User
module.exports.setUser = async (req, res) => {

	try	{

		let isAdminUser = await User.findByIdAndUpdate(req.params.userId, { isAdmin: false }, { new: true });

		if(isAdminUser){
			return res.send(true);
		}


	}	catch (error) {
		return res.send(false);
	}

}



// Retrieve authenticated user's orders
module.exports.retrieveOrders = async (req, res) => {

	try {

		return User.findById(req.user.id).then(result => {

			return res.send(result.orderedProduct.products);

		})

	} catch (error) {
		res.status(500).json({ error: 'Internal Server Error' });
	}

}



// Retrieve all orders
module.exports.retrieveAllOrders = async (req, res) => {

	try {

		let allUser = await User.find({});

		let allOrders = allUser.map(user => user.orderedProduct.products)
		.filter(orderedProduct => orderedProduct && orderedProduct.length > 0)
		.reduce((accumulator, currentArray) => accumulator.concat(currentArray), []);
		
		return res.send(allOrders);

	} catch (error) {
		res.status(500).json({ error: 'Internal Server Error' });
	}

}



// Edit user details
module.exports.editUser = async (req, res) => {

	try {

		console.log(req.files);

		let newDetails = {};

		    if (req.body.firstName) {
		      newDetails.firstName = req.body.firstName;
		    }

		    if (req.body.lastName) {
		      newDetails.lastName = req.body.lastName;
		    }

		    if (req.body.email) {
		      newDetails.email = req.body.email;
		    }

		    if (req.body.mobileNo) {
		      newDetails.mobileNo = req.body.mobileNo;
		    }
		    if (req.body.mobileNo) {
		      newDetails.mobileNo = req.body.mobileNo;
		    }
		    if (req.file) {

		    	newDetails.images = req.file.filename;

		    }
		      			    

		await User.findByIdAndUpdate(req.user.id, newDetails);

		return res.send(true);

	} catch (error) {

		console.log(error);

		res.status(500).json({ error: 'Internal Server Error' });

	}

}

module.exports.editPassword = async (req, res) => {


	try {

		const existingPass = await User.findById(req.params.userId);

		if (!existingPass) {

		      return res.status(404).json({ error: 'User not found' });

		}

		const isPasswordCorrect = bcrypt.compareSync(req.body.currentPass, existingPass.password);

		if (isPasswordCorrect) {

			const newPassword = req.body.newPass

			existingPass.password = await bcrypt.hash(newPassword, 10);

			await existingPass.save();

			return res.send(true);

		} else {

			return res.status(400).json({ error: 'Incorrect current password' });

		}

	} catch (error) {

		console.error(error);
		res.status(500).json({ error: 'Internal Server Error' });

	}



}



// Add shipping address
module.exports.addShippingAddress = async (req, res) => {

	try {

		let newAddress = {
			streetAddress: req.body.streetAddress,
			city: req.body.city,
			province: req.body.province,
			zipcode: req.body.zipcode
		}

		const address = await User.findById(req.user.id);

		address.shippingAddress.push(newAddress);
		await address.save();

		return res.send({ message: "Shipping Address added successfully" });

	} catch (error) {

		res.status(500).json({ error: 'Internal Server Error' });

	}

}



// Show all shipping address
module.exports.showAddress = async (req, res) => {

	try {

		return User.findById(req.user.id).then(result => {

			return res.send(result.shippingAddress);

		})

	} catch (error) {

		res.status(500).json({ error: 'Internal Server Error' });

	}

}



// Edit shipping address
module.exports.editShippingAddress = async (req, res) => {

	try {



		let editAddress = {
			$set: {
				'shippingAddress.$.streetAddress': req.body.streetAddress,
				'shippingAddress.$.city': req.body.city,
				'shippingAddress.$.province': req.body.province,
				'shippingAddress.$.zipcode': req.body.zipcode
			}
		}

		await User.findOneAndUpdate({_id: req.user.id, 'shippingAddress._id': req.params.addressId}, editAddress, { new: true });

		return res.send({ message: "Update successful" });

	} catch (error) {

		res.status(500).json({ error: 'Internal Server Error' });

	}

} 
