// [SECTION] Dependencies and Modules
const express = require("express");
const productController = require("../controllers/product");
const auth = require("../auth");
const upload = require('../multerConfig');

// destructure auth
const { verify, verifyAdmin } = auth;

// [SECTION] Routing Component
const router = express.Router();


// [SECTION] Routes

// Create a product
router.post("/create", verify, verifyAdmin, productController.createProduct);

// User checkout
router.post("/checkout", verify, productController.checkoutProduct);

// Retrieve all products
router.get("/all", productController.retrieveAllProducts);

// Retrieve all active products
router.get("/active", verify, productController.retrieveActiveProducts);

// Retrieve single product
router.get("/:productId", productController.retrieveAProduct);

// Update a product
router.put("/:productId", verify, verifyAdmin, productController.updateProduct);

// Archive a product
router.put("/:productId/archive", verify, verifyAdmin, productController.archiveProduct);

// Activate a product
router.put("/:productId/activate", verify, verifyAdmin, productController.activateProduct);

// [SECTION] Export Route System
module.exports = router;