// [SECTION] Dependencies and Modules
const express = require("express");
const cartController = require("../controllers/cart");
const auth = require("../auth");

// destructure auth
const { verify, verifyAdmin } = auth;

// [SECTION] Routing Component
const router = express.Router();

// [SECTION] Routes

// Add to cart
router.post("/add", verify, cartController.addToCart);

// Retrieve cart
router.get("/cart", verify, cartController.getCart);

// Change quantity of product in cart
router.put("/change-quantity", verify, cartController.changeQuantity);

// Remove products from cart
router.put("/:productId/remove", verify, cartController.removeFromCart);

// Checkout a product from cart
router.post("/checkout", verify, cartController.checkoutFromCart);

// Checkout all product from cart
router.post("/checkout-all", verify, cartController.checkoutAllFromCart);

// [SECTION] Export Route System
module.exports = router;
