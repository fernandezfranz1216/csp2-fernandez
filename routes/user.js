// [SECTION] Dependencies and Modules
const express = require("express");
const userController = require("../controllers/user");
const auth = require("../auth");
const upload = require('../multerConfig');


// destructure auth
const { verify, verifyAdmin } = auth;

// [SECTION] Routing Component
const router = express.Router();


// [SECTION] Routes

// User Registration
router.post("/register", upload.single('images'), userController.registerUser);

// User Authentication
router.post("/", userController.loginUser);

// Retrieve all users
router.get("/all", verify, verifyAdmin, userController.retrieveAllUsers);

// Retrieve user details
router.get("/details", verify, userController.userDetails);

// Set user as Admin
router.put("/:userId/set-admin", verify, verifyAdmin, userController.setAdmin);

// Set Admin as User
router.put("/:userId/set-user", verify, verifyAdmin, userController.setUser);

// Retrieve authenticated user's order
router.get("/orders", verify, userController.retrieveOrders);

// Retrieve all orders
router.get("/all-orders", verify, verifyAdmin, userController.retrieveAllOrders);

// Edit user details
router.put("/edit", verify, upload.single('images'), userController.editUser);

// Edit password
router.put("/:userId/edit-pass", verify, userController.editPassword);

// Add shipping address
router.post("/address", verify, userController.addShippingAddress);

// Show all shipping address
router.get("/address", verify, userController.showAddress);

// Edit shipping address
router.put("/:addressId/edit-address", verify, userController.editShippingAddress);


// [SECTION] Export Route System
module.exports = router;