// [SECTION]Dependencies and Modules
const express = require("express");
const mongoose = require("mongoose");
const multer = require('multer');
const path = require('path');

// "Cross Origin Resource Sharing"
// Allows our backend application to be available to our frontend application
const cors = require("cors");

// [SECTION] External Routes
const userRoutes = require("./routes/user");
const productRoutes = require("./routes/product");
const cartRoutes = require("./routes/cart");

// [SECTION]Environment setup
const port = 4004;

// [SECTION]Server setup
const app = express()

// Serve images from the specified directory
app.use('/images', express.static(path.join(__dirname, 'images')));


app.use(express.json({ limit: '5mb' }));
app.use(express.urlencoded({extended:true}));
// Allows our backend application to be available to our frontend application
app.use(cors());

// [SECTION] Database Connection
// Connect to our MongoDB database
mongoose.connect("mongodb+srv://fernandezfranz1216:admin123@cluster0.aku0oic.mongodb.net/thrift_shop?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});
// Prompts a message in the terminal once the connection is "open" and we are able to successfully connect to our database
mongoose.connection.once('open', () => console.log("Now connected to MongoDB Atlas."))


// [SECTION] Backend Routes
app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/carts", cartRoutes);


// [SECTION] Server Gateway Response
if(require.main === module){
	app.listen(port, () => {
		console.log(`API is now online on port ${port}`);
	})
}

module.exports = {app, mongoose};