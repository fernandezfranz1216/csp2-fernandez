## THRIFT SHOP API DOCUMENTATION

***INSTALLATION COMMAND:***

```npm install```

***TEST ACCOUNTS:***
- Regular User:
     - email: sample@mail.com
     - pwd: sample12345


- Admin User:
    - email: admin@mail.com
    - pwd: Pass12345
    

***USER ROUTES:***
1. register a user (POST)
	- http://localhost:4000/users/register
    - auth header required: NO
    - request body: 
        - email (string)
        - password (string)
        - firstName (string)
        - lastName (String)
        - mobileNo (number)
        - images (file)

2. login (POST)
	- http://localhost:4000/users/
    - auth header required: NO
    - request body: 
        - email (string)
        - password (string)

3. retrieve user details (GET)
	- http://localhost:4000/users/details
    - auth header required: YES
    - request body: none


4. retrieve all user details (Admin only) (GET)
    - http://localhost:4000/users/all
    - auth header required: YES
    - request body: none


5. set user as admin (Admin only) (PUT)
	- http://localhost:4000/users/:userId/set-admin
    - auth header required: YES
    - request body: none

6. set user as user (Admin only) (PUT)
    - http://localhost:4000/users/:userId/set-user
    - auth header required: YES
    - request body: none

7. retrieve authenticated user's orders (GET)
	- http://localhost:4000/users/orders
    - auth header required: YES
    - request body: none

8. retrieve all orders (Admin only) (GET)
	- http://localhost:4000/users/all-orders
    - auth header required: YES
    - request body: none

9. edit user information (PUT)
    - http://localhost:4000/users/edit
    - auth header required: YES
    - request body:
        - firstName (string)
        - lastName (string)
        - email (string)
        - mobileNo (string)
        - images (file)

10. edit user password (PUT)
    - http://localhost:4000/users/:userId/edit-pass
    - auth header required: YES
    - request body:
        - currentPass (string)
        - newPass (string)

11. create shipping address (POST)
    - http://localhost:4000/users/address
    - auth header required: YES
    - request body:
        - streetAddress (string)
        - city (string)
        - province (string)
        - zipcode (string)

12. get all shipping address (GET)
    - http://localhost:4000/users/address
    - auth header required: YES
    - request body: NONE


***PRODUCT ROUTES:***
13. create a product (Admin only) (POST)
	- http://localhost:4000/products/create
    - auth header required: YES
    - request body: 
        - name (string)
        - description (string)
        - price (number)

14. purchase a product (POST)
	- http://localhost:4000/products/checkout
    - auth header required: YES
    - request body: 
        - productId (string)
        - quantity (number)

15. retrieve all products (GET)
	- http://localhost:4000/products/all
    - auth header required: NONE
    - request body: none

16. retrieve active products (GET)
	- http://localhost:4000/products/active
    - auth header required: YES
	- request body: none

17. retrieve single product (GET)
	- http://localhost:4000/products/:productId	
    - auth header required: NONE
	- request body: none

18. update a product (Admin only) (PUT)
	- http://localhost:4000/products/:productId	
    - auth header required: YES
	- request body (any of the following):
		- email (string)
        - password (string)
        - retype password (string)
        - firstName (string)
        - lastName (String)
        - mobileNo (number)

19. archive a product (Admin only) (PUT)
	- http://localhost:4000/products/:productId/archive	
    - auth header required: YES
	- request body: none

20. activate a product (Admin only) (PUT)
	- http://localhost:4000/products/:productId/activate	
    - auth header required: YES
	- request body: none



***CART ROUTES:***

19. add to cart (POST)
	- http://localhost:4000/carts/add
    - auth header required: YES
    - request body: 
        - productId (string)
        - quantity (number)

20. change quantity (PUT)
	- http://localhost:4000/carts/change-quantity
    - auth header required: YES
    - request body: 
        - productId (string)
        - quantity (number)

21. remove product from cart (PUT)
	- http://localhost:4000/carts/:productId/remove
    - auth header required: YES
    - request body: none

22. checkout a product from cart (POST)
    - http://localhost:4000/carts/checkout
    - auth header required: YES
    - request body:
        - productId (string)

23. checkout all products from cart (POST)
    - http://localhost:4000/carts/checkout-all
    - auth header required: YES
    - request body: none